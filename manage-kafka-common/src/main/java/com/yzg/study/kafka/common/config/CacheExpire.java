package com.yzg.study.kafka.common.config;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * 过期缓存注解
 * @author Guowei
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheExpire {

    @AliasFor("expire")
    long value() default 60L;

    @AliasFor("value")
    long expire() default 60L;

    TimeType type() default TimeType.MINUTES;
}
