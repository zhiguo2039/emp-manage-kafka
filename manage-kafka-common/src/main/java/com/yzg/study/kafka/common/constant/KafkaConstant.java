package com.yzg.study.kafka.common.constant;

import lombok.Data;

import java.io.Serializable;

@Data
public class KafkaConstant implements Serializable {

    public static final String TOP_YZG = "topic-yzg";

    public static final String GROUP_YZG = "group-yzg";

}
