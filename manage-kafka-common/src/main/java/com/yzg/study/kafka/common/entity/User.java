package com.yzg.study.kafka.common.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "`user`")
public class User implements Serializable {
    @Id
    @Column(name = "`USER_ID`")
    private Long userId;

    @Column(name = "`USER_NAME`")
    private String userName;

    @Column(name = "`USER_TYPE`")
    private Integer userType;

    private static final long serialVersionUID = 1L;
}