package com.yzg.study.kafka.common.exception;

import lombok.ToString;

/**
 * 基础异常实体信息.
 * @author yzg
 */
@ToString
public class BaseException extends Exception{
    private static final long serialVersionUID = -778887391066124051L;
    protected String message;
    protected int code;

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public BaseException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BaseException(int code, String format, Object... args) {
        super(String.format(format, args));
        this.code = code;
        this.message = String.format(format, args);
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public int getCode() {
        return this.code;
    }
}
