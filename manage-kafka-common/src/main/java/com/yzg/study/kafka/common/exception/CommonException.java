package com.yzg.study.kafka.common.exception;

import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 通用异常实体信息.
 * @author yzg
 */
@ToString
@NoArgsConstructor
public class CommonException extends RuntimeException{

    private static final long serialVersionUID = -778887391066124051L;
    protected String message;
    protected int code;

    public CommonException(Throwable cause) {
        super(cause);
    }

    public CommonException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public CommonException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CommonException(int code, String format, Object... args) {
        super(String.format(format, args));
        this.code = code;
        this.message = String.format(format.replaceAll("\\{\\}", "%\\s"), args);
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public int getCode() {
        return this.code;
    }
}
