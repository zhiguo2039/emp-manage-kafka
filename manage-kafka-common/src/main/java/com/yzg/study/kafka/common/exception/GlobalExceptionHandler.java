package com.yzg.study.kafka.common.exception;

import com.yzg.study.kafka.common.base.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler({Exception.class})
    @ResponseBody
    public R<Exception> exception(Exception e) {
        logger.error("全局异常信息 ex={}", e.getMessage(), e);
        return R.fail("系统异常，请稍后重试");
    }

    @ExceptionHandler({ServiceException.class})
    @ResponseBody
    public R<Exception> exception(ServiceException e) {
        logger.error("全局异常信息 ex={}", e.getMessage(), e);
        return R.fail(e.getCode(), "请求服务异常，异常原因：{}", e.getMessage());
    }

    @ExceptionHandler({TokenInvalidException.class})
    @ResponseBody
    public R<Exception> exception(TokenInvalidException e) {
        logger.error("登陆异常 ex={}", e.getMessage(), e);
        return R.fail("请重新登录");
    }

}
