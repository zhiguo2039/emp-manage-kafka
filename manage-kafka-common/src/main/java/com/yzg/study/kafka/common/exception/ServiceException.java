package com.yzg.study.kafka.common.exception;

import com.yzg.study.kafka.common.exception.exceptioncode.BaseExceptionCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 服务异常实体信息.
 * @author yzg
 */
@ToString
@NoArgsConstructor
public class ServiceException extends CommonException {

    private static final long serialVersionUID = -3843907364558373817L;

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public ServiceException(String message) {
        super(-1, message);
    }

    public ServiceException(String message, Object... args) {
        super(-1, message, args);
    }

    public ServiceException(int code, String message) {
        super(code, message);
    }

    public ServiceException(int code, String message, Object... args) {
        super(code, message, args);
    }

    public static ServiceException wrap(int code, String message, Object... args) {
        return new ServiceException(code, message, args);
    }

    public static ServiceException wrap(String message, Object... args) {
        return new ServiceException(-1, message, args);
    }

    public static ServiceException validFail(String message, Object... args) {
        return new ServiceException(-9, message, args);
    }

    public static ServiceException wrap(BaseExceptionCode ex) {
        return new ServiceException(ex.getCode(), ex.getMsg());
    }
}
