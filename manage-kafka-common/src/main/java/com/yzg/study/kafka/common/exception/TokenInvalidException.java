package com.yzg.study.kafka.common.exception;

import com.yzg.study.kafka.common.exception.exceptioncode.BaseExceptionCode;

/**
 * token无效异常处理
 */
public class TokenInvalidException  extends CommonException {

    private static final long serialVersionUID = 5441584196737940880L;

    public TokenInvalidException(Throwable cause) {
        super(cause);
    }

    public TokenInvalidException(int code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public TokenInvalidException(String message) {
        super(-1, message);
    }

    public TokenInvalidException(int code, String message) {
        super(code, message);
    }

    public TokenInvalidException(int code, String message, Object... args) {
        super(code, message, args);
    }

    public static TokenInvalidException wrap(int code, String message, Object... args) {
        return new TokenInvalidException(code, message, args);
    }

    public static TokenInvalidException wrap(String message, Object... args) {
        return new TokenInvalidException(-1, message, args);
    }

    public static TokenInvalidException validFail(String message, Object... args) {
        return new TokenInvalidException(-9, message, args);
    }

    public static TokenInvalidException wrap(BaseExceptionCode ex) {
        return new TokenInvalidException(ex.getCode(), ex.getMsg());
    }
}
