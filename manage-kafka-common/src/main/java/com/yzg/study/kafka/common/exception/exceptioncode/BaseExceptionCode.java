package com.yzg.study.kafka.common.exception.exceptioncode;

/**
 * 基础异常编码.
 * @author yzg
 */
public interface BaseExceptionCode {
    int getCode();

    String getMsg();
}
