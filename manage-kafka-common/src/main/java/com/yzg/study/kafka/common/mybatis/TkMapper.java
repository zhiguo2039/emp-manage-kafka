package com.yzg.study.kafka.common.mybatis;

import com.yzg.study.kafka.common.mybatis.mapper.base.InsertListSelectiveMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * TKMapper实体
 * @author yzg
 */

public interface TkMapper<T> extends Mapper<T>, MySqlMapper<T>, InsertListSelectiveMapper<T> {
}
