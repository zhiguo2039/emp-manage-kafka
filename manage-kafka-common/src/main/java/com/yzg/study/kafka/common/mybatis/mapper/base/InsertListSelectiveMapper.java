package com.yzg.study.kafka.common.mybatis.mapper.base;

import java.util.List;

import com.yzg.study.kafka.common.mybatis.mapper.provider.SpecialProvider;
import org.apache.ibatis.annotations.InsertProvider;

public interface InsertListSelectiveMapper<T> {

    @InsertProvider(type = SpecialProvider.class, method = "dynamicSQL")
    int insertListSelective(List<T> records);

}
