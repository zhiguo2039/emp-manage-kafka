package com.yzg.study.kafka.common.quartz;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaScheduler {

    @Bean
    public JobDetail kafkaJobDetail(){
        return JobBuilder.newJob(KafkaJob.class)
                .withIdentity("KafkaJob").usingJobData("name","yzg")
                .storeDurably().build();
    }
}
