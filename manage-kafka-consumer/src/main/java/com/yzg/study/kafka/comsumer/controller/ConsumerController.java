package com.yzg.study.kafka.comsumer.controller;

import com.yzg.study.kafka.common.base.R;
import com.yzg.study.kafka.common.kafka.KafkaConsumerUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "kafka消费端接口设计")
@RestController
@Slf4j
public class ConsumerController {

    @Autowired
    private KafkaConsumerUtils consumerUtils;

    @GetMapping("/queryPartitionsByTopic")
    public R queryPartitionsByTopic(@RequestParam("topicName") String topicName){
        List<PartitionInfo> partitions = consumerUtils.queryPartitions(topicName);
        return new R(partitions);
    }

}
