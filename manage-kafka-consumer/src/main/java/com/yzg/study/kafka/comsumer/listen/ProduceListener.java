package com.yzg.study.kafka.comsumer.listen;

import com.yzg.study.kafka.common.constant.KafkaConstant;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
public class ProduceListener {

    @KafkaListener(groupId = KafkaConstant.GROUP_YZG, topics = KafkaConstant.TOP_YZG)
    public void process(ConsumerRecord<String, String> record, Acknowledgment acknowledgment){
        System.out.println("kafka接收的生产者消息为: topic = {"+ record.topic()+"}, offset = {"+ record.offset()+"}, value = {"+record.value()+"}");
        acknowledgment.acknowledge();
    }
}
