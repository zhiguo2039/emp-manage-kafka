package com.yzg.study.kafka.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.util.IdGenerator;
import tk.mybatis.spring.annotation.MapperScan;

@EnableDiscoveryClient
@RefreshScope
@SpringBootApplication(scanBasePackages = "com.yzg.study.kafka")
@MapperScan(basePackages = {"com.yzg.study.kafka.producer.mapper"})
@EnableCaching
public class ProducerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }

}
