package com.yzg.study.kafka.producer.controller;

import com.yzg.study.kafka.common.base.R;
import com.yzg.study.kafka.common.entity.User;
import com.yzg.study.kafka.common.utils.SnowFlakeUtil;
import com.yzg.study.kafka.producer.mapper.UserMapper;
import com.yzg.study.kafka.producer.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@Api(tags = "ShardingJDBC分表插入")
public class ShardingJdbcController {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserMapper userMapper;

    @PostMapping("/insertBatch")
    @ApiOperation("批量插入用户数据")
    public R insertBatch(@RequestBody List<User> userList) {
        for(User user : userList){
            user.setUserId(SnowFlakeUtil.generateId());
        }
        return R.success(userService.insert(userList));
    }

    @PostMapping("/selectAll")
    @ApiOperation("查询所有用户数据")
    public R selectAll() {
        return R.success(userService.selectAll());
    }

    @PostMapping("/insert")
    @ApiOperation("插入用户数据")
    public R insert() {
        for(int i = 0;i< 10;i++){
            User user = new User();
            user.setUserName("zj" + i);
            user.setUserType(i);
            userMapper.insertUser(user);
        }
        return R.success();
    }
}
