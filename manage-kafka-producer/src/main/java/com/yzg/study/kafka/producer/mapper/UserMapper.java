package com.yzg.study.kafka.producer.mapper;

import com.yzg.study.kafka.common.entity.User;
import com.yzg.study.kafka.common.mybatis.TkMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends TkMapper<User> {

    int insertUser(User user);

}