package com.yzg.study.kafka.producer.service;

import com.yzg.study.kafka.common.entity.User;

import java.util.List;

public interface IUserService {
    int insert(List<User> userList);

    List<User> selectAll();
}
