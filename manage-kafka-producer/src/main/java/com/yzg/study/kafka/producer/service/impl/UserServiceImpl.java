package com.yzg.study.kafka.producer.service.impl;

import com.yzg.study.kafka.common.config.CacheExpire;
import com.yzg.study.kafka.common.entity.User;
import com.yzg.study.kafka.producer.mapper.UserMapper;
import com.yzg.study.kafka.producer.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public int insert(List<User> userList) {
        return userMapper.insertListSelective(userList);
    }

    @Override
    @CacheExpire(1)
    @Cacheable("selectAll")
    public List<User> selectAll() {
        return userMapper.selectAll();
    }
}
